package uptop.me.testkotlin.dagger.components

import android.app.Application
import android.content.Context
import dagger.Component
import uptop.me.testkotlin.dagger.modules.ActivityModule
import uptop.me.testkotlin.dagger.modules.ApplicationModule
import uptop.me.testkotlin.dagger.modules.RestModule
import uptop.me.testkotlin.data.DataManager
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class, RestModule::class))
interface AppComponent {
    //    @ApplicationContext
    fun context(): Context

    fun application(): Application

    fun addActivityComponent(activityModule: ActivityModule): ActivityComponent

    fun providesDataManager(): DataManager

}
