package uptop.me.testkotlin.dagger.components

import dagger.Subcomponent
import uptop.me.testkotlin.ui.MainActivity
import uptop.me.testkotlin.dagger.modules.ActivityModule
import uptop.me.testkotlin.dagger.modules.FragmentModule
import uptop.me.testkotlin.dagger.scopes.PerActivity

@PerActivity
@Subcomponent(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {

    fun addFragmentComponent(fragmentModule: FragmentModule): FragmentComponent

    fun inject(mainActivity: MainActivity)

}
