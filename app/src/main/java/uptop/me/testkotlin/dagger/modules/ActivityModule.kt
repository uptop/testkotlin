package uptop.me.testkotlin.dagger.modules

import android.app.Activity
import android.content.Context
import dagger.Module
import dagger.Provides
import uptop.me.testkotlin.dagger.ActivityContext

@Module
class ActivityModule(private val activity: Activity) {


    //    @Provides
    //    MainPresenter providePresenter(MainModel model) {
    //        return new MainPresenter(model);
    //    }

    @Provides
    internal fun providesActivity(): Activity {
        return activity
    }

    @Provides
    @ActivityContext
    internal fun providesContext(): Context {
        return activity
    }
}
