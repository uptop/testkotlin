package uptop.me.testkotlin.dagger.components

import dagger.Subcomponent
import uptop.me.testkotlin.dagger.modules.FragmentModule
import uptop.me.testkotlin.dagger.scopes.PerFragment

@PerFragment
@Subcomponent(modules = arrayOf(FragmentModule::class))
interface FragmentComponent {
//    fun inject(fragment: MainFragment)
}
