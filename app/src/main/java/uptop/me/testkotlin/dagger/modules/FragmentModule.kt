package uptop.me.testkotlin.dagger.modules


import android.content.Context
import android.support.v4.app.Fragment
import dagger.Module
import dagger.Provides
import uptop.me.testkotlin.dagger.FragmentContext

@Module
class FragmentModule(private val fragment: Fragment) {

    @Provides
    internal fun providesFragment(): Fragment {
        return fragment
    }


    @Provides
    @FragmentContext
    internal fun providesContext(): Context {
        return fragment.context
    }


}
