package uptop.me.testkotlin.utils

object ConstantManager {
    val MAX_CONNECTION_TIMEOUT = 5000
    val MAX_READ_TIMEOUT = 5000
    val MAX_WRITE_TIMEOUT = 5000

    val BASE_URL = "http://www.umori.li/api/"
}
