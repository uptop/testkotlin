package uptop.me.testkotlin.data

import uptop.me.testkotlin.data.network.RestService
import uptop.me.testkotlin.data.storage.CountryDto
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataManager @Inject
constructor(private val mPreferencesManager: PreferenceManager,
            private val mRestService: RestService) {
    private var mMockProductList: MutableList<CountryDto>? = null


    val dataForAdapter: List<CountryDto>?
        get() = mMockProductList

    init {
        generateMockData()
    }

    private fun generateMockData() {
        mMockProductList = ArrayList()
        mMockProductList!!.add(CountryDto("Россия", 150, 5))
    }

    companion object {
        private val INSTANCE: DataManager? = null
    }
}
