package uptop.me.testkotlin.data.storage

import android.os.Parcel
import android.os.Parcelable

class CountryDto : Parcelable {
    var name: String? = null
    var quantityOfPeople: Int = 0
    var rating: Int = 0

    constructor(name: String, quantityOfPeople: Int, rating: Int) {
        this.name = name
        this.quantityOfPeople = quantityOfPeople
        this.rating = rating
    }


    protected constructor(`in`: Parcel) {
        name = `in`.readString()
        quantityOfPeople = `in`.readInt()
        rating = `in`.readInt()
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeString(name)
        parcel.writeInt(quantityOfPeople)
        parcel.writeInt(rating)
    }

    companion object {

        val CREATOR: Parcelable.Creator<CountryDto> = object : Parcelable.Creator<CountryDto> {
            override fun createFromParcel(`in`: Parcel): CountryDto {
                return CountryDto(`in`)
            }

            override fun newArray(size: Int): Array<CountryDto?> {
                return arrayOfNulls(size)
            }
        }
    }
}
