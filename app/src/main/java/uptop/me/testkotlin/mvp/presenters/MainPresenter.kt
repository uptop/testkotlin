package uptop.me.testkotlin.mvp.presenters

import uptop.me.testkotlin.data.DataManager
import uptop.me.testkotlin.mvp.views.MainContract
import javax.inject.Inject

//extends AbstractPresenter<MainContract.View> implements MainContract.Presenter

class MainPresenter @Inject constructor(dataManager: DataManager): AbstractPresenter<MainContract.View>(), MainContract.Presenter {
    private lateinit var mDataManager: DataManager

    init {
        mDataManager = dataManager
    }

    override fun getDataForAdapter() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}

