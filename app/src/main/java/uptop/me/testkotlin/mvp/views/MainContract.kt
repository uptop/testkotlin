package uptop.me.testkotlin.mvp.views

import uptop.me.testkotlin.data.storage.CountryDto
import uptop.me.testkotlin.mvp.presenters.BasePresenter

interface MainContract {

    interface View : BaseView<Presenter> {

        fun initAdapter(data: List<CountryDto>)
    }

    interface Presenter : BasePresenter {

        fun getDataForAdapter()
    }
}