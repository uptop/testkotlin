package uptop.me.testkotlin.mvp.presenters;


import uptop.me.testkotlin.mvp.views.BaseView;

public class AbstractPresenter<T extends BaseView> {
    private T mView;

    public void setView(T view) {
        mView = view;
    }

    public T getView() {
        return mView;
    }
}
