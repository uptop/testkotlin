package uptop.me.testkotlin

import android.app.Application

import uptop.me.testkotlin.dagger.components.AppComponent
import uptop.me.testkotlin.dagger.components.DaggerAppComponent
import uptop.me.testkotlin.dagger.modules.ApplicationModule


class TestKotlinApp : Application() {

    val component: AppComponent?
        get() {
            if (applicationComponent == null) {
                applicationComponent = DaggerAppComponent.builder()
                        .applicationModule(ApplicationModule(this))
                        .build()
            }

            return applicationComponent
        }

    override fun onCreate() {
        super.onCreate()
        application = this
    }

    companion object {
        var applicationComponent: AppComponent? = null
        var application: TestKotlinApp? = null
    }
}
