package uptop.me.testkotlin.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import uptop.me.testkotlin.R
import uptop.me.testkotlin.TestKotlinApp
import uptop.me.testkotlin.dagger.components.ActivityComponent
import uptop.me.testkotlin.dagger.modules.ActivityModule
import uptop.me.testkotlin.mvp.presenters.MainPresenter
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
     @Inject
     lateinit var mPresenter: MainPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        createDaggerComponent().inject(this)
    }

    private fun createDaggerComponent(): ActivityComponent {
        return TestKotlinApp.application!!.component!!
                .addActivityComponent(ActivityModule(this))
    }
}
